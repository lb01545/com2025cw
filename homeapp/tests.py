from django.test import TestCase
from django.urls import reverse
from .models import *
from .forms import *

class HomePageTests(TestCase):

    def setUp(self):

        return

    def test_homepage(self):
        response = self.client.get('')
        self.assertEqual(response.status_code, 200)

        self.assertContains(response, 'Home Page')
        self.assertContains(response, 'Copyright @ 2022 LiamBull, Inc.')


    def test_contact(self):
        response = self.client.get(reverse('contact'))
        self.assertEqual(response.status_code, 200)
        self.assertContains(response, 'Contact Us')
        self.assertContains(response, 'Copyright @ 2022 LiamBull, Inc.')  

    # tests if mailer successfully sends form to database - data is valid
    def test_mailer(self):
        data = {
            "name": "John",
            "email": "John@email.co.uk",
            "subject": "A subject",
            "message": "This is a message",
        }
        form = ContactForm(data)
        self.assertTrue
        (form.is_valid())

    # tests if mailer does not input an invalid contact form - email is invalid
    def test_incorrect_mailer(self):
        data = {
            "name": "John",
            "email": "Not an email",
            "subject": "A subject",
            "message": "This is a message",
        }
        form = ContactForm(data)
        self.assertFalse
        (form.is_valid())