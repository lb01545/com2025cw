from django.test import TestCase
from django.db.backends.sqlite3.base import IntegrityError
from django.db import transaction
from .models import *
from django.urls import reverse
from .forms import *

class AlbumTests(TestCase):
    @classmethod
    def setUpTestData(cls):
        a1 = Album(title='Album 1', creator="This is the 1st creator", release_year="2000", number_of_songs="6")
        a1.save()
        a2 = Album(title='Album 2', creator="This is the 2nd creator", release_year="2001", number_of_songs="4")
        a2.save()

    def test_save_album(self):
        db_count = Album.objects.all().count()
        album = Album(title='new Album', creator="New creator", release_year="2000", number_of_songs="8")
        album.save()
        self.assertEqual(db_count+1, Album.objects.all().count())
        

    def test_post_create(self):
        db_count = Album.objects.all().count()
        data = {
        "title": "new album",
        "creator": " new creator",
        "release_year": "2000",
        "number_of_songs": "10",
        }
        response = self.client.post(reverse('albums_new'), data=data)
        self.assertEqual(Album.objects.count(), db_count+1)

    # tests that invalid release year prevents album sent to database
    def test_invalid_year(self):
        data = {
        "title": "new album",
        "creator": " new creator",
        "release_year": "-2022",
        "number_of_songs": "10",
        }
        form = AlbumForm(data)
        self.assertFalse
        (form.is_valid())

    # tests that invalid no. of songs prevents album sent to database
    def test_invalid_song_num(self):
        data = {
        "title": "new album",
        "creator": " new creator",
        "release_year": "2010",
        "number_of_songs": "-8",
        }
        form = AlbumForm(data)
        self.assertFalse
        (form.is_valid())


    ################
    # Song testing #
    ################

    # tests that a valid song is sent to database
    def test_post_create_song(self):
        album = Album.objects.get(pk=1)
        data = {
        "title": "new song",
        "favourite": True,
        "album": album
        }
        form = SongForm(data)
        self.assertTrue
        (form.is_valid())
        
    # tests that an invalid song with no title is not sent to database
    def test_post_create_empty_song(self):
        data = {
        "title": "",
        "favourite": True,
        "album": Album.objects.get(pk=1)
        }
        form = SongForm(data)
        self.assertFalse
        (form.is_valid())        

    
    ################
    # Note testing #
    ################

    # tests that a valid note is sent to database
    def test_post_create_note(self):
        album = Album.objects.get(pk=1)
        data = {
        "description": "a new note",
        "album": album
        }
        form = NoteForm(data)
        self.assertTrue
        (form.is_valid())

    # tests that an invalid note with no description is not sent to database
    def test_post_create_empty_note(self):
        album = Album.objects.get(pk=1)
        data = {
        "description": "",
        "album": album
        }
        form = NoteForm(data)
        self.assertFalse
        (form.is_valid())