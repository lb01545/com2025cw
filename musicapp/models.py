from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator

class Album(models.Model):
    album_img = models.ImageField(upload_to='images/', default='images/default.JPG')
    title = models.CharField(max_length = 128)
    creator = models.CharField(max_length = 128)
    # Validation here allows for suitable data entry to the database
    release_year = models.PositiveIntegerField(validators=[MinValueValidator(1888), MaxValueValidator(2022)])
    number_of_songs = models.PositiveIntegerField(validators=[MinValueValidator(1)])

class Song(models.Model):
    title = models.CharField(max_length = 128)
    favourite = models.BooleanField(default=False)
    album = models.ForeignKey(Album, on_delete=models.CASCADE)

class Note(models.Model):
    description = models.TextField()
    album = models.ForeignKey(Album, on_delete=models.CASCADE)
