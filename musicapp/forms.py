from django import forms
from .models import Album, Song, Note

# creating a form
class AlbumForm(forms.ModelForm):

    # create meta class
    class Meta:
        # specify model to be used
        model = Album

        fields = ['album_img', 'title', 'creator','release_year','number_of_songs']
    
        widgets = {
            'title': forms.TextInput(attrs={
            'class': 'formfield',
            'placeholder': 'Album Title',
            'required': 'True',
            }),
            'creator': forms.TextInput(attrs={
            'class': 'formfield',
            'placeholder': 'Album Creator',
            'required': 'True',
            }),
            'release_year': forms.NumberInput(attrs={
            'class': 'formfield',
            'required': 'True',
            }),
            'number_of_songs': forms.NumberInput(attrs={
            'class': 'formfield',
            'required': 'True',
            }),
        }

class SongForm(forms.ModelForm):
    # create meta class
    class Meta:
        # specify model to be used
        model = Song
        fields = ['title', 'favourite', 'album']
        widgets = {
        'title': forms.TextInput(attrs={
        'class': 'formfield',
        'placeholder': 'Song Title',
        }),
        'album': forms.HiddenInput(),
}

class NoteForm(forms.ModelForm):
    # create meta class
    class Meta:
        # specify model to be used
        model = Note
        fields = ['description', 'album']
        widgets = {
        'description': forms.Textarea(attrs={
        'class': 'formfield',
        'placeholder': 'Note Description',
        'rows' : 25,
        'cols' : 60,
        }),
        'album': forms.HiddenInput(),
}