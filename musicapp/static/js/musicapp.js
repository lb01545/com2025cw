async function togglefavourite(song_id){
    $.ajax({
        url: '/albums/togglefavourite',
        type: 'get',
        data: {
            song_id: song_id,
        },
        success: function(response){

            if(response.favourite == true)
            {
                $("#sid_"+response.sid).removeClass("not_favourite");
                $("#sid_"+response.sid).addClass("favourite");
            }
            else
            {
                $("#sid_"+response.sid).removeClass("favourite");
                $("#sid_"+response.sid).addClass("not_favourite");
            }
        }
    });
}
    
async function delete_s(song_id){
    $.ajax({
        url: '/albums/deletesong',
        type: 'get',
        data: {
            song_id: song_id,
        },
        success: function(response){
            if(response.delete_success == true)
            {
                $("#sid_"+response.sid).hide();
            }
        }
    });
} 

async function delete_n(note_id){
    $.ajax({
        url: '/albums/deletenote',
        type: 'get',
        data: {
            note_id: note_id,
        },
        success: function(response){
            if(response.delete_success == true)
            {
                $("#nid_"+response.nid).hide();
            }
        }
    });
} 