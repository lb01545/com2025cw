from django.views.generic import ListView, CreateView, DetailView, View
from django.shortcuts import (get_object_or_404, render, redirect)
from django.http import HttpResponse, JsonResponse
from .forms import AlbumForm, SongForm, NoteForm
from .models import Album, Song, Note
from django.urls import reverse_lazy
from django.contrib import messages

def index_view(request):
    # dictionary for initial data with
    # field names as keys
    context ={}

    # add the dictionary during initialization
    context["album_list"] = Album.objects.all()
    return render(request, "musicapp/index.html", context)


class AlbumDetailView(DetailView):
    model = Album
   
    template_name = 'musicapp/detail_view.html'
    
    def get_context_data(self, **kwargs):
        context = {}
        context['album'] = Album.objects.get(id=self.kwargs['pk'])
        context['song_list'] = Song.objects.filter(album__id=self.kwargs['pk'])
        context['note_list'] = Note.objects.filter(album__id=self.kwargs['pk'])
        
        return context


def create_view(request):
    context ={}
    form = AlbumForm(request.POST or None, request.FILES or None)
    if(request.method == 'POST'):
        if form.is_valid():
            form.save()
            messages.add_message(request, messages.SUCCESS, 'Album Created')
            return redirect('albums_index')
        else:
            messages.add_message(request, messages.ERROR, 'Invalid Form Data; Album not created')
    
    context['form']= form
    return render(request, "musicapp/create_view.html", context) 


def update_view(request, nid):
    context ={}

    # fetch the object related to passed id
    obj = get_object_or_404(Album, id = nid)

    # pass the object as instance in form
    form = AlbumForm(request.POST or None, request.FILES or None, instance = obj)

    # save the data from the form and
    # redirect to detail_view
    if form.is_valid():
        form.save()
        messages.add_message(request, messages.SUCCESS, 'Album Updated')
        return redirect('albums_detail', pk=nid)

    # add form dictionary to context
    context["form"] = form
    return render(request, "musicapp/update_view.html", context)


def delete_view(request, nid):
    # fetch the object related to passed id
    obj = get_object_or_404(Album, id = nid)
    # delete object
    obj.delete()
    messages.add_message(request, messages.SUCCESS, 'Album Deleted')
    # after deleting redirect to index view
    return redirect('albums_index')


class SongListView(ListView):
    model = Song
    template_name = 'musicapp/song_list.html'
    context_object_name = 'song_list'

    def get_queryset(self):
        return Song.objects.filter(album__id=self.kwargs['nid']) 


class CreateSongView(CreateView):
    model = Song
    form_class = SongForm
    template_name = "musicapp/create_view.html"
    
    def get_initial(self): # set the initial value of our album field
        album = Album.objects.get(id=self.kwargs['nid'])
        return {'album': album}
        
    def get_success_url(self): # redirect to the album detail view on success
        return reverse_lazy('albums_detail', kwargs={'pk':self.kwargs['nid']})

class FavouriteSongView(View):
    def get(self, request):
        sid = request.GET.get('song_id')
        song = get_object_or_404(Song, pk=sid)
        song.favourite = not(song.favourite)
        song.save()
        return JsonResponse({'favourite': song.favourite, 'sid': sid}, status=200)

class DeleteSongView(View):
    def get(self, request):
        sid = request.GET.get('song_id')
        try:
            song = Song.objects.get(pk=sid)
        except Song.DoesNotExist:
            return JsonResponse({'delete_success': False, 'sid': sid}, status=200)
        song.delete()
        return JsonResponse({'delete_success': True, 'sid': sid}, status=200)


class NoteListView(ListView):
    model = Note
    template_name = 'musicapp/note_list.html'
    context_object_name = 'note_list'

    def get_queryset(self):
        return Note.objects.filter(album__id=self.kwargs['nid']) 

class CreateNoteView(CreateView):
    model = Note
    form_class = NoteForm
    template_name = "musicapp/create_view.html"
    
    def get_initial(self): # set the initial value of our album field
        album = Album.objects.get(id=self.kwargs['nid'])
        return {'album': album}
        
    def get_success_url(self): # redirect to the album detail view on success
        return reverse_lazy('albums_detail', kwargs={'pk':self.kwargs['nid']})


class DeleteNoteView(View):
    def get(self, request):
        nid = request.GET.get('note_id')
        try:
            note = Note.objects.get(pk=nid)
        except Song.DoesNotExist:
            return JsonResponse({'delete_success': False, 'nid': nid}, status=200)
        note.delete()
        return JsonResponse({'delete_success': True, 'nid': nid}, status=200)