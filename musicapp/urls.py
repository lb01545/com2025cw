from django.urls import path
from . import views

urlpatterns = [
    #albums/
    path('', views.index_view, name='albums_index'),
    #albums/id
    path('<int:pk>', views.AlbumDetailView.as_view(), name='albums_detail'),
    #albums/new
    path('new', views.create_view, name='albums_new'),
    #albums/edit/id
    path('edit/<int:nid>', views.update_view, name='albums_update'),
    #albums/delete/id
    path('delete/<int:nid>', views.delete_view, name='albums_delete'),
    #albums/id/songs/
    path('<int:nid>/songs', views.SongListView.as_view(), name='song_list'),
    #albums/id/song/new
    path('<int:nid>/song/new', views.CreateSongView.as_view(), name='create_song'),
    #/albums/togglefavourite
    path('togglefavourite', views.FavouriteSongView.as_view(), name='favourite_song'),
    #/albums/deletesong
    path('deletesong', views.DeleteSongView.as_view(), name='delete_song'),
    #albums/id/notes/
    path('<int:nid>/notes', views.NoteListView.as_view(), name='note_list'),
    #albums/id/note/new
    path('<int:nid>/note/new', views.CreateNoteView.as_view(), name='create_note'),
    #/albums/deletenote
    path('deletenote', views.DeleteNoteView.as_view(), name='delete_note'),
]